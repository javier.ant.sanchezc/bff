package com.bootcamp.coders.bff.client;

import com.bootcamp.bff_openapiv3.project.api.model.CRMConsultResponse;
import com.bootcamp.bff_openapiv3.project.api.model.CRMDeleteResponse;
import com.bootcamp.bff_openapiv3.project.api.model.CRMRegisterResponse;
import com.bootcamp.bff_openapiv3.project.api.model.CustomerInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(value = "crm", url = "${bff.url.crm}")
public interface CustomerCrmClient {

    @PostMapping(value = "/register")
    ResponseEntity<CRMRegisterResponse> registerClient(CustomerInfo body);

    @GetMapping(value = "/all")
    ResponseEntity<CRMConsultResponse> getAllClients();

    @DeleteMapping(value = "/remove/{id}")
    ResponseEntity<CRMDeleteResponse> removeClient(@PathVariable Long id);
}
