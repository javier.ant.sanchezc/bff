package com.bootcamp.coders.bff.controller;


import com.bootcamp.bff_openapiv3.project.api.CustomerCrmApi;
import com.bootcamp.bff_openapiv3.project.api.model.CRMConsultResponse;
import com.bootcamp.bff_openapiv3.project.api.model.CRMDeleteResponse;
import com.bootcamp.bff_openapiv3.project.api.model.CRMRegisterResponse;
import com.bootcamp.bff_openapiv3.project.api.model.CustomerInfo;
import com.bootcamp.coders.bff.client.CustomerCrmClient;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BffCustomerController implements CustomerCrmApi {
    final static Log logger = LogFactory.getLog(BffCustomerController.class);

    private final CustomerCrmClient customerCrmClient;

    public BffCustomerController(CustomerCrmClient customerCrmClient) {

            this.customerCrmClient = customerCrmClient;






    }


    @Override
    public ResponseEntity<CRMConsultResponse> getAllClients() {
        try {
            logger.info("Asignacion de la peticion");
          this.customerCrmClient.getAllClients();
        }
        catch (Exception e)
        {
            logger.error("Error en la peticion "+e);
        }
        return this.customerCrmClient.getAllClients();

    }

    @Override
    public ResponseEntity<CRMRegisterResponse> registerClient(CustomerInfo customerInfo) {
        try {
            logger.info("Asignacion de la peticion");
            this.customerCrmClient.registerClient(customerInfo);
        }
        catch (Exception e)
        {
            logger.error("Error en la peticion "+e);
        }

        return this.customerCrmClient.registerClient(customerInfo);
    }

    @Override
    public ResponseEntity<CRMDeleteResponse> removeClient(Long id) {
        try {
            logger.info("Asignacion de la peticion");
             this.customerCrmClient.removeClient(id);
        }
        catch (Exception e)
        {
            logger.error("Error en la peticion "+e);
        }
        return this.customerCrmClient.removeClient(id);
    }
}
