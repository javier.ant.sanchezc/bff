package com.bootcamp.coders.bff.controller;

import com.bootcamp.bff_openapiv3.project.api.AuthApi;
import com.bootcamp.bff_openapiv3.project.api.BlackListApi;
import com.bootcamp.bff_openapiv3.project.api.model.AuthenticationRequest;
import com.bootcamp.bff_openapiv3.project.api.model.AuthenticationResponse;
import com.bootcamp.bff_openapiv3.project.api.model.BlackListResponse;
import com.bootcamp.bff_openapiv3.project.api.model.CustomerInfo;
import com.bootcamp.coders.bff.client.AuthClient;
import com.bootcamp.coders.bff.client.RestrictiveListsClient;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;

@RestController
public class BffRestrictiveListsController implements BlackListApi, AuthApi {

    private final RestrictiveListsClient restrictiveListsClient;
    private final AuthClient authClient;
    private String jwt;

    public BffRestrictiveListsController(RestrictiveListsClient restrictiveListsClient, AuthClient authClient) {
        this.restrictiveListsClient = restrictiveListsClient;
        this.authClient = authClient;
    }

    @Override
    public ResponseEntity<AuthenticationResponse> authenticate(AuthenticationRequest authenticationRequest) {
        ResponseEntity<AuthenticationResponse> authenticationResponse = this.authClient.authenticate(authenticationRequest);
        jwt = "Bearer " + Objects.requireNonNull(authenticationResponse.getBody()).getJwt();
        return authenticationResponse;
    }

    @Override
    public ResponseEntity<BlackListResponse> consultBlackListed(CustomerInfo customerInfo) {
        if(jwt != null) return this.restrictiveListsClient.isBlackListed(jwt, customerInfo);
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
}
