package com.bootcamp.coders.bff.client;

import com.bootcamp.bff_openapiv3.project.api.model.AuthenticationRequest;
import com.bootcamp.bff_openapiv3.project.api.model.AuthenticationResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value = "auth", url = "${bff.url.blacklist}")
public interface AuthClient {

    @RequestMapping(method = RequestMethod.POST, value = "/auth")
    ResponseEntity<AuthenticationResponse> authenticate(AuthenticationRequest body);
}
