package com.bootcamp.coders.bff.client;

import com.bootcamp.bff_openapiv3.project.api.model.BlackListResponse;
import com.bootcamp.bff_openapiv3.project.api.model.CustomerInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value = "restrictivelists", url = "http://localhost:8081/blacklist")
public interface RestrictiveListsClient {

    @RequestMapping(method = RequestMethod.POST, value = "/search")
    ResponseEntity<BlackListResponse> isBlackListed(@RequestHeader("Authorization") String token, CustomerInfo body);
}
