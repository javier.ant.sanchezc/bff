package com.bootcamp.coders.bff.controller;

import com.bootcamp.coders.bff.client.CustomerCrmClient;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(BffCustomerController.class)
class BffCustomerControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    CustomerCrmClient customerCrmClient;


    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void getAllClients() throws Exception {

        this.mvc.perform(get("/all"))
                   .andExpect(status().isOk());


    }

    @Test
    void registerClient() throws Exception {
        this.mvc.perform(get("/register"))
                .andExpect(status().isOk());
    }


    @Test
    void removeClient() throws Exception {
       //this.mvc.perform(get("/all/remove/{id}"))
              //  .andExpect(status().isOk());
    }
}